package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;




public class TC002_CreateLead extends Annotations {
	

@BeforeTest
public void setData(){
	testcaseName = "TC002_CreateLead";
	testcaseDec = "Create Lead and verify";
	author = "Monisha";
	category = "smoke";
	//excelFileName = "TC001";
} 


@Test	
public void createLead() {
		new LoginPage()
		.enterUserName()
		.enterPassWord()
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickSubmitCreateLead()
		.verifyFirstName();
	
	
	

}
}

