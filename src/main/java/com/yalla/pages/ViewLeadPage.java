package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using = "viewLead_firstName_sp") WebElement eleVerifyFirstName;
	@FindBy(how=How.PARTIAL_LINK_TEXT, using = "Edit") WebElement eleClickEdit;
	@FindBy(how=How.ID, using = "viewLead_lastName_sp") WebElement eleVerifyLastName;
	
	public ViewLeadPage verifyFirstName() {
		String firstName = eleVerifyFirstName.getText();
		
		if(firstName.equals("Monisha"))
		{
			System.out.println("Updated First Name successfully");
		}
		return this;
		
	}
	
	public UpdatePage clickEdit() {
		click(eleClickEdit);
		return new UpdatePage();
	}
	
	public ViewLeadPage verifyLastName() {
		String lastName = eleVerifyLastName.getText();
		if(lastName.equals("S"))
		{
			System.out.println("Updated Last Name successfully");
		}
		return this;
	}
	
	

}
