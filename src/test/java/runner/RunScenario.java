package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\java\\bdd\\createLead_Egs_.feature",
                 glue = "steps",
                 monochrome = true        
)

public class RunScenario {
	

}
