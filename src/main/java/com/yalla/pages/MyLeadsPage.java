package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	public MyLeadsPage() {
		
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.PARTIAL_LINK_TEXT, using = "Create Lead") WebElement eleCreateLead;
	@FindBy(how=How.PARTIAL_LINK_TEXT, using = "Find Leads") WebElement eleClickFindLeads;
	
	public CreateLeadPage clickCreateLead() {
		
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	public FindLeadsPage clickFindLeads() {
		click(eleClickFindLeads);
		return new FindLeadsPage();
		
	}


}
