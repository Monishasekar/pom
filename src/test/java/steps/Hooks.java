package steps;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class Hooks extends SeleniumBase{

	@Before
	public void beforeScenario(Scenario sc) {
		System.out.println(sc.getName());
		System.out.println(sc.getId());
	}
	
	@After
	public void afterScenarios(Scenario sc) {
		System.out.println(sc.getStatus());
	}
}
