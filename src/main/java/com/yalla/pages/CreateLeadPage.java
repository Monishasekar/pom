package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage() {
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using = "createLeadForm_companyName") WebElement eleCompName; 
	@FindBy(how=How.ID, using = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.NAME, using = "submitButton") WebElement clickSubmitCreateLead;
	
	
	public CreateLeadPage enterCompanyName() {
		
		clearAndType(eleCompName, "Wipro");
		return this;
		
	}
	
	
	public CreateLeadPage enterFirstName() {
		clearAndType(eleFirstName, "Monisha");
	    return this; 
	}
	
	public CreateLeadPage enterLastName() {
		clearAndType(eleLastName, "Sekar");
		return this;
	}
	
	public ViewLeadPage clickSubmitCreateLead()	{
		click(clickSubmitCreateLead);
		return new ViewLeadPage();
	}

}
