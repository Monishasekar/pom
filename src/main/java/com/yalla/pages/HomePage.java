package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 
  
	@FindBy(how=How.PARTIAL_LINK_TEXT, using = "CRM/SFA") WebElement eleClickCrmsfa;
	
	public MyHomePage clickCRMSFA() {
		
		click(eleClickCrmsfa);
		return new MyHomePage();
	}
}







