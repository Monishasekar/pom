package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{
	
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using = "(//input[@name = 'firstName'])[3]") WebElement eleEnterFirstName;
	@FindBy(how=How.XPATH, using = "//button[text() = 'Find Leads']") WebElement eleClickFindLeadsButton;
	@FindBy(how=How.PARTIAL_LINK_TEXT, using = "Monisha") WebElement eleClickLeadList; 		
	
	
	public FindLeadsPage enterLeadID() {
		clearAndType(eleEnterFirstName, "Monisha");
		return this;
	}
			
	public FindLeadsPage clickFindLeadsButton() {
		click(eleClickFindLeadsButton);
		return this;
	}
	
	public ViewLeadPage clickLeadList() {
		click(eleClickLeadList);
		return new ViewLeadPage();
	}
}
