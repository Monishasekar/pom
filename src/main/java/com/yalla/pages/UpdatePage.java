package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class UpdatePage extends Annotations{

	public UpdatePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using = "updateLeadForm_lastName") WebElement eleEditLastName;
	@FindBy(how=How.CLASS_NAME, using = "smallSubmit") WebElement eleClickUpdateButton;
	
	public UpdatePage editLastName() {
		clearAndType(eleEditLastName, "S");
		return this;
	}
	
	public ViewLeadPage clickUpdateButton() {
		click(eleClickUpdateButton);
		return new ViewLeadPage();
	}
}
